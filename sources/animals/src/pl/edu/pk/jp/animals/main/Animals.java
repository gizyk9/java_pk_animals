package pl.edu.pk.jp.animals.main;

import java.util.ArrayList;
import java.util.List;

/*import pl.edu.pk.jp.animals.Animal;
import pl.edu.pk.jp.animals.Cat;
import pl.edu.pk.jp.animals.Dog;
*/

import pl.edu.pk.jp.animals.*;

public class Animals {

	private static List<Animal> list = new ArrayList<Animal>();
	
	public static void main(String[] args) {
		
		list.add(new Dog());
		list.add(new Dog());
		list.add(new Cat());
		
		for(int i=0; i<list.size(); i++)
			System.out.println(list.get(i).say());
		
	}

}
